Backed by one of the industry's best customer service programs, we take great pride in setting the pace in which Edmonton home builders strive to maintain. Pacesetter Homes is an industry leader with over 50 years of home building experience and a wide variety of beautiful, award-winning home plans.

Address: 3203 93 St NW, #160, Edmonton, AB T6N 0B2, Canada

Phone: 780-733-7399